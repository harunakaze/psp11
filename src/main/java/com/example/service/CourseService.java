package com.example.service;

import com.example.model.CourseModel;

import java.util.List;

/**
 * Created by harunakaze on 11-Mar-17.
 */
public interface CourseService {
    CourseModel selectCourse (String idCourse);


    List<CourseModel> selectAllCourses ();


    void addCourse (CourseModel course);


    void deleteCourse (String idCourse);


    void updateCourse(CourseModel course);
}
