package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by harunakaze on 14-May-17.
 */
@Controller
public class PageController {
    @RequestMapping("/")
    public String index ()
    {
        return "index";
    }

    @RequestMapping("/login")
    public String login ()
    {
        return "login";
    }

}
