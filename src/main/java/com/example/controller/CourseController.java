package com.example.controller;

import com.example.model.CourseModel;
import com.example.model.StudentModel;
import com.example.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class CourseController {

    @Autowired
    CourseService courseService;

    @RequestMapping("/course/view/{idCourse}")
    public String viewPath (Model model,
                            @PathVariable(value = "idCourse") String idCourse)
    {
        CourseModel course = courseService.selectCourse (idCourse);

        if (course != null) {
            model.addAttribute ("course", course);
            return "viewCourse";
        } else {
            model.addAttribute ("idCourse", idCourse);
            return "not-found-course";
        }
    }

    @RequestMapping("/course/viewall")
    public String viewall(Model model) {
        List<CourseModel> courses = courseService.selectAllCourses();
        model.addAttribute("courses", courses);

        return "viewallcourses";
    }
}
